#!/bin/bash

# Função para mudar a cor do PS1
change_color() {
    local color=$1
    export COLOR="\[\033[${color}m\]"
    export RESET="\[\033[0m\]"
}

# Função para exibir apenas o nome do usuário no PS1
display_username() {
    export PS1="$COLOR\u$RESET\n$"
}

# Função para ter um prompt em duas linhas
two_line_prompt() {
    export PS1="$COLOR\u@\h \w$RESET\n$"
}

# Função para voltar ao prompt inicial
reset_prompt() {
    export PS1="\u@\h \W\$"
}
