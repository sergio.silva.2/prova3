#!/bin/bash
source func.sh

echo "Escolha a customização:"
echo "1. Mudar a cor"
echo "2. Exibir apenas o nome do usuário"
echo "3. Prompt em duas linhas"
echo "4. Resetar para o prompt inicial"

read choice

case $choice in
    1)
        echo "Escolha uma cor (por exemplo, 31 para vermelho): "
        read color
        change_color $color
        ;;
    2)
        display_username
        ;;
    3)
        two_line_prompt
        ;;
    4)
        reset_prompt
        ;;
    *)
        echo "Opção inválida"
        ;;
esac
